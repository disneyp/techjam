import requests
json_distance = {
"first_pos": {"x": 3, "y": -2},
"second_pos": {"x": -1, "y": 1}
}

r = requests.post("http://localhost:8000/distance",json=json_distance)
print(r.text, r.status_code)

pos1 = {'position' : {'x' : 0, 'y' : 0}}
r = requests.put("http://localhost:8000/robot/1/position",json=pos1)
print(r.text, r.status_code)

r = requests.get("http://localhost:8000/robot/1/position/")
print(r.text, r.status_code)

r = requests.get("http://localhost:8000/robot/2/position")
print(r.text, r.status_code)

pos2 = {'position' : {'x' : 5, 'y' : -2}}
r = requests.put("http://localhost:8000/robot/2/position",json=pos2)
print(r.text, r.status_code)

robot_dist = {
"first_pos" : "robot#1",
"second_pos" : "robot#2"
}

r = requests.post("http://localhost:8000/distance",json=robot_dist)
print(r.text, r.status_code)

robot_dist1 = {
"first_pos" : "robot#1",
"second_pos" : "robot#3"
}

r = requests.post("http://localhost:8000/distance",json=robot_dist1)
print(r.text, r.status_code)

d1 = {}
r = requests.post("http://localhost:8000/distance",json=d1)
print(r.text, r.status_code)

d2 = 'asdfasdf'
r = requests.post("http://localhost:8000/distance",json=d2)
print(r.text, r.status_code)

json_distance = {
"first_pos": {"x": 3, "y": -2, "z'" : 40},
"second_pos": {"x": -1, "y": 1}
}
r = requests.post("http://localhost:8000/distance",json=json_distance)
print(r.text, r.status_code)

json_distance = {
"first_pos": {"x": 3, "y": -2, "z'" : 40},
"second_pos": {"x": -1, "y": 1},
"metric" : "manhattan"
}
r = requests.post("http://localhost:8000/distance",json=json_distance)
print(r.text, r.status_code)


pos = {'ref_position' : {'x' : 16, 'y' : 20}}
r = requests.post("http://localhost:8000/nearest",json=pos)
print(r.text, r.status_code)




alien_pos1 = {'robot_id' : 1, 'distance' : 3.162}
r = requests.post("http://localhost:8000/alien/abcdefghijklmnop/report", json=alien_pos1)
print(r.text, r.status_code)

alien_pos2 = {'robot_id' : 2, 'distance' : 2.236}
r = requests.post("http://localhost:8000/alien/abcdefghijklmnop/report", json=alien_pos2)
print(r.text, r.status_code)

r = requests.get("http://localhost:8000/alien/abcdefghijklmnop/position")
print(r.text, r.status_code)

pos3 = {'position' : {'x' : 3, 'y' : -3}}
r = requests.put("http://localhost:8000/robot/3/position",json=pos3)
print(r.text, r.status_code)

alien_pos3 = {'robot_id' : 3, 'distance' : 2.0}
r = requests.post("http://localhost:8000/alien/abcdefghijklmnop/report", json=alien_pos3)
print(r.text, r.status_code)

r = requests.get("http://localhost:8000/alien/abcdefghijklmnop/position")
print(r.text, r.status_code)
