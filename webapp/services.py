import operator
import ast
import re
from math import sqrt
from http import HTTPStatus
from flask import Flask, jsonify, request
from shapely.geometry import Point
import regex
from collections import defaultdict


app = Flask(__name__)

robots = {} # {robot_name : (x,y)}
metric = "euclidean"
alien = defaultdict(dict)

def RepresentsInt(s):
    try:
        int(s)
        if '.' in str(s):
            return False
        return True
    except ValueError:
        return False

def RepresentsFloat(s):
    try:
        float(s)
        return True
    except ValueError:
        return False

def id_to_name(robot_id):
    return "robot#" + str(robot_id)

def is_robot(P1):
    if isinstance(P1, str):
        robot_name_pattern = regex.compile("^robot#([1-9][0-9]*)$")
        if regex.match(robot_name_pattern, P1):
            return True
        return False
    return False

def get_position(P):
    if is_robot(P):
        if P not in robots.keys():
            return None
        return robots[P]
    return P['x'], P['y']

def abs(x):
    return max(x, -x)

def compute_distance(x1, y1, x2, y2, metric="euclidean"):
    if metric == "euclidean":
        return sqrt(sq(x1-x2)+sq(y1-y2))
    else:
        return abs(x1-x2) + abs(y1-y2)

def is_dict_with_keys(d, key_list):
    if not isinstance(d, dict): return False
    for k in key_list:
        if k not in d.keys():
            return False
    return True

@app.route("/distance/", methods=['POST'])
def calculate_distance():
    body = request.get_json()
    # if not isinstance(body, dict) or 'first_pos' not in body.keys() or 'second_pos' not in body.keys():
    #     return '', HTTPStatus.BAD_REQUEST
    if not is_dict_with_keys(body, ['first_pos', 'second_pos']):
        return '', HTTPStatus.BAD_REQUEST

    P1 = body['first_pos']
    P2 = body['second_pos']
    if not is_dict_with_keys(P1, ['x', 'y']) and not is_robot(P1):
        return '', HTTPStatus.BAD_REQUEST
    if not is_dict_with_keys(P2, ['x', 'y']) and not is_robot(P2):
        return '', HTTPStatus.BAD_REQUEST


    pos1 = get_position(P1)
    pos2 = get_position(P2)
    # trying to get unknown robot id
    if pos1 is None or pos2 is None:
        return '', HTTPStatus.FAILED_DEPENDENCY
    x1, y1 = pos1
    x2, y2 = pos2

    if not RepresentsInt(x1) or not RepresentsInt(y1) or not RepresentsInt(x2) or not RepresentsInt(y2):
        return '', HTTPStatus.BAD_REQUEST

    metric = 'euclidean' # default
    if 'metric' in body.keys():
        metric = body['metric']
        if metric not in ['euclidean', 'manhattan']:
            return '', HTTPStatus.BAD_REQUEST
        print('metric', metric)

    result = compute_distance(x1, y1, x2, y2, metric=metric)
    return jsonify(distance=result), HTTPStatus.OK

def sq(a):
    return a*a
'''
def check_point(pos):
    if isinstance(pos, dict):
        if 'x' in pos.keys() and 'y' in pos.keys():
            return True
        return False
    return False
'''
def is_robot_id(robot_id):
    if not RepresentsInt(robot_id): return False
    if int(robot_id) <= 0 or int(robot_id) >= 1000000:
        return False
    return True

@app.route("/robot/<robot_id>/position/", methods=['PUT'])
def put_robot_pos(robot_id):
    if not is_robot_id(robot_id):
        return '', HTTPStatus.BAD_REQUEST

    robot_name = id_to_name(robot_id)
    body = request.get_json()

    if not is_dict_with_keys(body, ['position']):
        return '', HTTPStatus.BAD_REQUEST

    pos = body['position']
    print(pos, flush=True)
    if not is_dict_with_keys(pos, ['x', 'y']):
        return '', HTTPStatus.BAD_REQUEST
    if not RepresentsInt(pos['x']) or not RepresentsInt(pos['y']):
        return '', HTTPStatus.BAD_REQUEST


    robots[robot_name] = (pos['x'], pos['y'])
    print('robots=', robots, flush=True)
    return '', HTTPStatus.NO_CONTENT

@app.route("/robot/<robot_id>/position/", methods=['GET'])
def get_robot_pos(robot_id):
    if not is_robot_id(robot_id):
        return '', HTTPStatus.BAD_REQUEST
    robot_name = id_to_name(robot_id)
    print(robot_name, flush=True)
    if robot_name in robots.keys():
        pos = robots[robot_name]
        pos_json = {"x" : pos[0], "y" : pos[1]}
        print(pos, flush=True)
        return jsonify(position=pos_json), HTTPStatus.OK
    return '', HTTPStatus.NOT_FOUND

def greater_robot_id(robot_id1, robot_id2):
    if int(robot_id1[len('robot#'):]) > int(robot_id2[len('robot#'):]):
        return True
    return False

class Circle:
    def __init__(self, x, y, r):
        self.x = x
        self.y = y
        self.r = r

    def __str__(self):
        return "(" + ",".join([str(self.x), str(self.y), str(self.r)]) + ")"

@app.route("/alien/<object_dna>/report", methods=["POST"])
def put_alien(object_dna):
    object_dna_pattern = regex.compile('^[a-z]{16}$')
    if not regex.match(object_dna_pattern, object_dna):
        return '', HTTPStatus.BAD_REQUEST

    body = request.get_json()
    if not is_dict_with_keys(body, ['robot_id', 'distance']):
        return '', HTTPStatus.BAD_REQUEST

    robot_id = body['robot_id']
    if not is_robot_id(robot_id):
        return '', HTTPStatus.BAD_REQUEST

    robot_name = id_to_name(robot_id)

    if not RepresentsFloat(body['distance']):
        return '', HTTPStatus.BAD_REQUEST

    dist = float(body['distance'])

    alien[object_dna][robot_name] = dist
    return '', HTTPStatus.OK

def norm(v):
    return sqrt(v[0] ** 2 + v[1] ** 2)

def normalize(v):
    norm_v = norm(v)
    return (v[0]/norm_v, v[1]/norm_v)


# returns set of pts
def intersect_circs(c1, c2):
    x1, y1, r1 = c1.x, c1.y, c1.r
    x2, y2, r2 = c2.x, c2.y, c2.r
    print('computing inter:', str(c1), str(c2))

    if x1 - x2 < 1e-3 and y1-y2 < 1e-3and r1-r2 < 1e-3:
        # same circ
        return None
    d = compute_distance(x1, y1, x2, y2)
    if d > r1 + r2 - 1e-3 or d < max(r1,r2) - min(r1,r2) + 1e-3:
        # too far
        return set()
    x = (d**2 + r1**2 - r2**2)/(d*2)
    a = sqrt((-d+r1-r2) * (-d-r1+r2) * (-d+r1+r2) * (d+r1+r2)) / d
    v = normalize((x2-x1, y2-y1))
    perp_v = (-v[1], v[0])
    print('x', x, 'a', a, 'v', v, flush=True)
    print('perp_v', perp_v, flush=True)

    tmp = (x1 + x * v[0], y1 + x * v[1])
    p1 = (tmp[0] + perp_v[0] * a/2, tmp[1] + perp_v[1] * a /2)
    p2 = (tmp[0] - perp_v[0] * a/2, tmp[1] - perp_v[1] * a /2)

    pp1 = (round(p1[0], 2), round(p1[1], 2))
    pp2 = (round(p2[0], 2), round(p2[1], 2))
    # re-check
    if compute_distance(pp1[0], pp1[1], x1, y1) - r1 > 1e-2:
        pp1 = None
    if compute_distance(pp1[0], pp1[1], x2, y2) - r2 > 1e-2:
        pp1 = None
    if compute_distance(pp2[0], pp2[1], x1, y1) - r1 > 1e-2:
        pp2 = None
    if compute_distance(pp2[0], pp2[1], x2, y2) - r2 > 1e-2:
        pp2 = None

    if pp1 is None and pp2 is None:
        return set()
    if pp1 is None:
        return set([pp2])
    if pp2 is None:
        return set([pp1])
    return set([pp1, pp2])


@app.route("/alien/<object_dna>/position", methods=["GET"])
def get_alien_pos(object_dna):
    object_dna_pattern = regex.compile('^[a-z]{16}$')
    if not regex.match(object_dna_pattern, object_dna):
        return '', HTTPStatus.BAD_REQUEST

    if object_dna not in alien.keys():
        return '', HTTPStatus.FAILED_DEPENDENCY

    d = alien[object_dna]
    key_list = d.keys()
    if len(key_list) == 1:
        return '', HTTPStatus.FAILED_DEPENDENCY

    circles = []
    for robot_name in d:
        x, y = robots[robot_name]
        r = d[robot_name]
        circles.append(Circle(x,y,r))

    print('circles', circles)

    n = len(circles)
    inter = None
    for i in range(n):
        for j in range(i+1,n):
            tmp = intersect_circs(circles[i], circles[j])
            print('intersections:', tmp)
            if tmp is None:
                continue
            else:
                if inter is None:
                    inter = tmp
                else:
                    inter = inter & tmp

    inter = list(inter)
    print('inter',inter, flush=True)
    if len(inter) > 1:
        return '', HTTPStatus.FAILED_DEPENDENCY

    pos = inter[0]
    pos_json = {'x' : int(pos[0]), 'y' : int(pos[1])}
    return jsonify(position=pos_json), HTTPStatus.OK




@app.route("/nearest", methods=['POST'])
def get_nearest_robot():
    body = request.get_json()
    # check in keys
    if not is_dict_with_keys(body, ['ref_position']):
        return '', HTTPStatus.BAD_REQUEST


    pos = body['ref_position']
    if not is_dict_with_keys(pos, ['x', 'y']):
        return '', HTTPStatus.BAD_REQUEST
    if not RepresentsInt(pos['x']) or not RepresentsInt(pos['y']):
        return '', HTTPStatus.BAD_REQUEST

    if len(robots.keys()) == 0:
        return jsonify(robot_ids=[]), HTTPStatus.OK
    min_dist = None
    min_robot_id = None
    for k in robots:
        robot_x, robot_y = robots[k]
        dist = compute_distance(pos['x'], pos['y'], robot_x, robot_y)
        if min_dist is None or min_dist > dist:
            min_dist = dist
            min_robot_id = k
        elif min_dist == dist:
            if greater_robot_id(min_robot_id, k):
                min_robot_id = k
    return jsonify(robot_id=[int(min_robot_id[len('robot#'):])]), HTTPStatus.OK

@app.route("/closestpair/", methods=["GET"])
def closest():
    xs = []
    ys = []
    for k in robots:
        xs.append(robots[k][0])
        ys.append(robots[k][1])

    if len(xs) <= 1:
        return '', HTTPStatus.FAILED_DEPENDENCY

    closest_dist = solution(xs, ys)
    print('closest', closest_dist)
    return jsonify(distance=closest_dist), HTTPStatus.OK

def dist1(p1, p2):
    return sqrt((p1[0] - p2[0]) ** 2 + (p1[1] - p2[1]) ** 2)

def brute(ax):
    mi = dist1(ax[0], ax[1])
    p1 = ax[0]
    p2 = ax[1]
    ln_ax = len(ax)
    if ln_ax == 2:
        return p1, p2, mi
    for i in range(ln_ax-1):
        for j in range(i + 1, ln_ax):
            if i != 0 and j != 1:
                d = dist1(ax[i], ax[j])
                if d < mi:  # Update min_dist and points
                    mi = d
                    p1, p2 = ax[i], ax[j]
    return p1, p2, mi

def closest_split_pair(p_x, p_y, delta, best_pair):
    ln_x = len(p_x)  # store length - quicker
    mx_x = p_x[ln_x // 2][0]  # select midpoint on x-sorted array
    # Create a subarray of points not further than delta from
    # midpoint on x-sorted array
    s_y = [x for x in p_y if mx_x - delta <= x[0] <= mx_x + delta]
    best = delta  # assign best value to delta
    ln_y = len(s_y)  # store length of subarray for quickness
    for i in range(ln_y - 1):
        for j in range(i+1, min(i + 7, ln_y)):
            p, q = s_y[i], s_y[j]
            dst = dist1(p, q)
            if dst < best:
                best_pair = p, q
                best = dst
    return best_pair[0], best_pair[1], best

def closest_pair(ax, ay):
    ln_ax = len(ax)  # It's quicker to assign variable
    if ln_ax <= 3:
        return brute(ax)  # A call to bruteforce comparison
    mid = ln_ax // 2  # Division without remainder, need int
    Qx = ax[:mid]  # Two-part split
    Rx = ax[mid:]
    # Determine midpoint on x-axis
    midpoint = ax[mid][0]
    Qy = list()
    Ry = list()
    for x in ay:  # split ay into 2 arrays using midpoint
        if x[0] <= midpoint:
           Qy.append(x)
        else:
           Ry.append(x)
    # Call recursively both arrays after split
    (p1, q1, mi1) = closest_pair(Qx, Qy)
    (p2, q2, mi2) = closest_pair(Rx, Ry)
    # Determine smaller distance between points of 2 arrays
    if mi1 <= mi2:
        d = mi1
        mn = (p1, q1)
    else:
        d = mi2
        mn = (p2, q2)
    # Call function to account for points on the boundary
    (p3, q3, mi3) = closest_split_pair(ax, ay, d, mn)
    # Determine smallest distance for the array
    if d <= mi3:
        return mn[0], mn[1], d
    else:
        return p3, q3, mi3

def solution(x, y):
    a = list(zip(x, y))  # This produces list of tuples
    ax = sorted(a, key=lambda x: x[0])  # Presorting x-wise
    ay = sorted(a, key=lambda x: x[1])  # Presorting y-wise
    p1, p2, mi = closest_pair(ax, ay)  # Recursive D&C function
    return mi



if __name__ == '__main__':
    json_distance = """{"x":3, "y":2}"""
    calculate_distance(json_distance)
