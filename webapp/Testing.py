import requests
json_distance = {
"first_pos": {"x": 0, "y": 0},
"second_pos": {"x": 1, "y": 1}
}
#basic distance of 2 point
r = requests.post("http://localhost:8000/distance",json=json_distance)
print(r.text, r.status_code)



r = requests.get("http://localhost:8000/closestpair")
print(r.text, r.status_code)

#1 code 200
ref4 = {
  "ref_position": {
    "x": 4,
    "y": 4
  }
}
r = requests.post("http://localhost:8000/nearest",json=ref4)
print(r.text, r.status_code)
#2 200 []
#Feature A Correct Testing
#put robot 1
robot1 = {
  "position": {
    "x": 2,
    "y": 2
  }
}
r = requests.put("http://localhost:8000/robot/1/position",json=robot1)
print(r.text, r.status_code)
#3 code 204



r = requests.get("http://localhost:8000/closestpair")
print(r.text, r.status_code)

#get robot 1
r = requests.get("http://localhost:8000/robot/1/position")
print(r.text, r.status_code)
#4 code 200

#get robot 2
r = requests.get("http://localhost:8000/robot/2/position")
print(r.text, r.status_code)
#5 code 404

json_distance2 = {
"first_pos": "robot#1",
"second_pos": {"x": 1, "y": 1}
}

#A distance of point / robot
r = requests.post("http://localhost:8000/distance",json=json_distance2)
print(r.text, r.status_code)
#6 code 200 result root2


json_distance3 = {
"first_pos": "robot#2",
"second_pos": {"x": 1, "y": 1}
}
r = requests.post("http://localhost:8000/distance",json=json_distance3)
print(r.text, r.status_code)
#7 424

#Feature B Correct Testing
json_mathattan = {
"first_pos": {"x": 3, "y": -2},
"second_pos": {"x": 1, "y": 1},
"metric" : "manhattan"
}
r = requests.post("http://localhost:8000/distance",json=json_mathattan)
print(r.text, r.status_code)
#8 code 200

#Feature C Correct Testing
#Robot 1 : 2 2
#Robot 2 : 4 4

#put robot 2
robot2 = {
  "position": {
    "x": 4,
    "y": 4
  }
}
r = requests.put("http://localhost:8000/robot/2/position",json=robot2)
print(r.text, r.status_code)
#9 code 204

ref1 = {
  "ref_position": {
    "x": 2,
    "y": 3
  }
}
r = requests.post("http://localhost:8000/nearest",json=ref1)
print(r.text, r.status_code)
#10 200 result 1

ref2 = {
  "ref_position": {
    "x": 3,
    "y": 3
  }
}
r = requests.post("http://localhost:8000/nearest",json=ref2)
print(r.text, r.status_code)
#11 200 result 1

ref3 = {
  "ref_position": {
    "x": 4,
    "y": 4
  }
}
r = requests.post("http://localhost:8000/nearest",json=ref3)
print(r.text, r.status_code)
#12 200 result 2

#Feature D Correct testing

robot4 = {
"position": {"x": 0, "y": 0}
}
r = requests.put("http://localhost:8000/robot/4/position",json=robot4)
print(r.text, r.status_code)
#13 code 204

a1 = {
"robot_id": 4,
"distance": 3.162
}
print('=====14=====')
r = requests.post("http://localhost:8000/alien/abcdefghijklmnop/report",json=a1)
print(r.text, r.status_code)
#14 code 200

robot5 = {
"position": {"x": 5, "y": -2}
}
r = requests.put("http://localhost:8000/robot/5/position",json=robot5)
print(r.text, r.status_code)
#15 code 204

a2 = {
"robot_id": 5,
"distance": 2.236
}
r = requests.post("http://localhost:8000/alien/abcdefghijklmnop/report",json=a2)
print(r.text, r.status_code)
#16 code 200

r = requests.get("http://localhost:8000/alien/abcdefghijklmnop/position")
print(r.text, r.status_code)
#17 code 424

robot6 = {
"position": {"x": 3, "y": -3}
}
r = requests.put("http://localhost:8000/robot/6/position",json=robot6)
print(r.text, r.status_code)
#18 code 204

a3 = {
"robot_id": 6,
"distance": 2.000
}
r = requests.post("http://localhost:8000/alien/abcdefghijklmnop/report",json=a3)
print(r.text, r.status_code)
#19 code 200

r = requests.get("http://localhost:8000/alien/abcdefghijklmnop/position")
print(r.text, r.status_code)
#20 code 200 3 -1



print('=====closest======')
r = requests.get("http://localhost:8000/closestpair")
print(r.text, r.status_code)


s = "asdad"
json_nokey = {
"first_pos": {"x": 0, "y": 0},
}
json_keyer = {
"first_pos": {"x": 0, "y": 0},
"second_pos" : {}
}

json_keynoint = {
"first_pos": {"x": 0, "y": 0},
"second_pos" : {"x":1, "y":1.43}
}

json_manu = {
"first_pos": {"x": 0, "y": 0},
"second_pos" : {"x":1, "y":1},
"metric" : "manunited"
}
r = requests.post("http://localhost:8000/distance",json=s)
print(r.text, r.status_code)
#1 code 400 - error non json

r = requests.post("http://localhost:8000/distance",json=json_nokey)
print(r.text, r.status_code)
#2 code 400 - error no key

r = requests.post("http://localhost:8000/distance",json=json_keyer)
print(r.text, r.status_code)
#3 code 400 - error key error

r = requests.post("http://localhost:8000/distance",json=json_keynoint)
print(r.text, r.status_code)
#4 code 400 - error no int

r = requests.post("http://localhost:8000/distance",json=json_manu)
print(r.text, r.status_code)
#5 code 400 - error mahhattan
